﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Try5.Startup))]
namespace Try5
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
