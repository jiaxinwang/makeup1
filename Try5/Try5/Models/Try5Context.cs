﻿using Try5.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Try5.DAL
{
    public class Try5Context : DbContext
    {

        public Try5Context() : base("Try5Context")
        {
        }

        public DbSet<Post> Post { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}