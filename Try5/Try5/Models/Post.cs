﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Try5.Models
{
    public class Post
    {
        public int ID { get; set; }
        public string Item { get; set; }
        public string Detail { get; set; }
        public List<Post> Comment { get; set; }
    }
}